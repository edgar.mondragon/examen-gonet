/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  edgarmondragon
 * Created: 12-ene-2021
 */

DROP TABLE IF EXISTS oauth_access_token;

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` mediumblob,
  `authentication_id` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
);

DROP TABLE IF EXISTS oauth_client_token;
CREATE TABLE `oauth_client_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ;

DROP TABLE IF EXISTS oauth_code;
CREATE TABLE `oauth_code` (
  `code` varchar(255) DEFAULT NULL,
  `authentication` blob
) ;

DROP TABLE IF EXISTS oauth_refresh_token;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` mediumblob,
  `authentication` blob
);

INSERT INTO user (user_id, password, role_id, username) VALUES (1, '$2a$08$POCMskqkWdVT9Y7mf8DLgeqpI300tbNVuf9zrBpfOYHvWxDLRE2D2', 1, 'examen_gonet');



