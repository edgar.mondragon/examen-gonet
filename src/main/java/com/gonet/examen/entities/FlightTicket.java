/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gonet.examen.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author edgarmondragon
 */
@Entity
public class FlightTicket implements Serializable {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long flightTicketId;
    private Date fechaSalida;
    private Date fechaLlegada;
    private String ciudadOrigen;
    private String ciudadDestino;
    private String nombrePasajero;
    private long edadPasajero;
    private boolean tieneBodegaEquipaje;
    private double precio;
    private String horaSalida;
    private String horaLlegada;

    /**
     * @return the flightTicketId
     */
    public Long getFlightTicketId() {
        return flightTicketId;
    }

    /**
     * @param flightTicketId the flightTicketId to set
     */
    public void setFlightTicketId(Long flightTicketId) {
        this.flightTicketId = flightTicketId;
    }

    /**
     * @return the fechaSalida
     */
    public Date getFechaSalida() {
        return fechaSalida;
    }

    /**
     * @param fechaSalida the fechaSalida to set
     */
    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    /**
     * @return the fechaLlegada
     */
    public Date getFechaLlegada() {
        return fechaLlegada;
    }

    /**
     * @param fechaLlegada the fechaLlegada to set
     */
    public void setFechaLlegada(Date fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    /**
     * @return the ciudadOrigen
     */
    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    /**
     * @param ciudadOrigen the ciudadOrigen to set
     */
    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    /**
     * @return the ciudadDestino
     */
    public String getCiudadDestino() {
        return ciudadDestino;
    }

    /**
     * @param ciudadDestino the ciudadDestino to set
     */
    public void setCiudadDestino(String ciudadDestino) {
        this.ciudadDestino = ciudadDestino;
    }

    /**
     * @return the nombrePasajero
     */
    public String getNombrePasajero() {
        return nombrePasajero;
    }

    /**
     * @param nombrePasajero the nombrePasajero to set
     */
    public void setNombrePasajero(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }

    /**
     * @return the edadPasajero
     */
    public long getEdadPasajero() {
        return edadPasajero;
    }

    /**
     * @param edadPasajero the edadPasajero to set
     */
    public void setEdadPasajero(long edadPasajero) {
        this.edadPasajero = edadPasajero;
    }

    /**
     * @return the tieneBodegaEquipaje
     */
    public boolean isTieneBodegaEquipaje() {
        return tieneBodegaEquipaje;
    }

    /**
     * @param tieneBodegaEquipaje the tieneBodegaEquipaje to set
     */
    public void setTieneBodegaEquipaje(boolean tieneBodegaEquipaje) {
        this.tieneBodegaEquipaje = tieneBodegaEquipaje;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the horaSalida
     */
    public String getHoraSalida() {
        return horaSalida;
    }

    /**
     * @param horaSalida the horaSalida to set
     */
    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    /**
     * @return the horaLlegada
     */
    public String getHoraLlegada() {
        return horaLlegada;
    }

    /**
     * @param horaLlegada the horaLlegada to set
     */
    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }
}
