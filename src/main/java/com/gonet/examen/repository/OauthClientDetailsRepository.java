/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gonet.examen.repository;

import com.gonet.examen.entities.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author edgarmondragon
 */
public interface OauthClientDetailsRepository extends JpaRepository<OauthClientDetails, Long> {
    
}
