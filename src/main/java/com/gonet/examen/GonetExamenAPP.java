/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gonet.examen;


import com.gonet.examen.entities.OauthClientDetails;
import com.gonet.examen.repository.OauthClientDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
/**
 *
 * @author edgar
 */
@SpringBootApplication(scanBasePackages={"com.gonet.examen"})
public class GonetExamenAPP implements CommandLineRunner {
    
    private @Autowired OauthClientDetailsRepository oauthClientDetailsRepository;
    
    public static void main(String[] args) {
        SpringApplication.run(GonetExamenAPP.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        /*PasswordEncoder oauthClientPasswordEncoder = new BCryptPasswordEncoder(4);
        PasswordEncoder userPasswordEncoder = new BCryptPasswordEncoder(8);
        System.out.println(userPasswordEncoder.encode("examen_gonet"));
        System.out.println(oauthClientPasswordEncoder.encode("examen_gonet"));*/
        PasswordEncoder oauthClientPasswordEncoder = new BCryptPasswordEncoder(4);
        OauthClientDetails det = new OauthClientDetails();
        det.setAccess_token_validity(108000);
        det.setAdditional_information("");
        det.setAuthorities("ADMIN");
        det.setAuthorized_grant_types("password,authorization_code,refresh_token,implicit");
        det.setAutoapprove("");
        det.setClientId("examen_gonet");
        det.setClient_secret(oauthClientPasswordEncoder.encode("examen_gonet"));
        det.setRefresh_token_validity(2592000);
        det.setResource_ids("resource-server-rest-api");
        det.setScope("read,write");
        det.setWeb_server_redirect_uri("");
        oauthClientDetailsRepository.save(det);
    }
    
}