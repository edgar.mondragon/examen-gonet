/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gonet.examen.controllers;

import com.gonet.examen.entities.FlightTicket;
import com.gonet.examen.exception.ResourceNotFoundException;
import com.gonet.examen.repository.FlightTicketRepository;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author edgarmondragon
 */

@RestController
@RequestMapping("/flightTicket")
public class FlightTicketController {
    private static final Logger LOGGER = LogManager.getLogger(FlightTicketController.class);
    private @Autowired FlightTicketRepository flightTicketRepository;
    
    //<editor-fold defaultstate="collapsed" desc="Crea boleto de vuelo">
    @ApiOperation(value = "Crea boleto de vuelo, regresa boleto creado",response = Iterable.class)
    @PutMapping("/create")
    public FlightTicket create(@RequestBody FlightTicket flightTicket){
        return flightTicketRepository.save(flightTicket);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Regresa boleto de acuerdo a id">
    @ApiOperation(value = "Regresa boleto de acuerdo a id",response = Iterable.class)
    @GetMapping("/getById/{flightTicketId}")
    public FlightTicket getById(@PathVariable("flightTicketId") long flightTicketId) throws ResourceNotFoundException {
        try {
            return flightTicketRepository.findById(flightTicketId).get();
        }catch(Exception ex){
            LOGGER.error(ex.toString());
        }
        throw new ResourceNotFoundException("", "", "Boleto no encontrado.");
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Regresa todos los boletos">
    @ApiOperation(value = "Regresa todos los boletos",response = Iterable.class)
    @GetMapping("/getAll")
    public List getAll(){
        return flightTicketRepository.findAll();
    }
    //</editor-fold>
}
