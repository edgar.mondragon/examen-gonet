/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gonet.examen.controllers;

import com.gonet.examen.exception.ResourceNotAutorizedException;
import com.gonet.examen.model.AccesToken;
import com.gonet.examen.model.LoginModel;
import com.gonet.examen.entities.User;
import com.gonet.examen.repository.UserRepository;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author edgar
 */

@RestController
@RequestMapping("/login")
public class LoginController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
    private @Autowired UserRepository userRepository;
    @Value("${config.oauth2.accessUri}")
    private String oauthUrl;
    private final Gson gson = new Gson();
    
    @PostMapping("/getUser")
    public AccesToken getUser(@RequestBody LoginModel login) throws ResourceNotAutorizedException {
        AccesToken token;
        User user;
        try {
            URL url = new URL (oauthUrl + "oauth/token");
            String urlParameters  = "grant_type=password&username=" + login.getUser() + "&password=" + login.getPass();
            byte[] postData = urlParameters.getBytes();
            int postDataLength = postData.length;
            String encoding = Base64.getEncoder().encodeToString(("examen_gonet:examen_gonet").getBytes());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
            connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty( "charset", "utf-8");
            connection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
            connection.setUseCaches( false );
            try( DataOutputStream wr = new DataOutputStream( connection.getOutputStream())) {
                wr.write( postData );
            }
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in  = new BufferedReader (new InputStreamReader (content));
            String line;
            String resp = "";
            while ((line = in.readLine()) != null) {
                resp += line;
            }
            if(resp.contains("html")){
                throw new ResourceNotAutorizedException("", "", "Usuario o contraseña no validos.");
            }
            token = gson.fromJson(resp, AccesToken.class);
            user = userRepository.findByUsername(login.getUser());
            user.setPassword("private");
            token.setUser(user);
        } catch(IOException e) {
            LOGGER.error(e.toString());
            throw new ResourceNotAutorizedException("", "", "Usuario o contraseña no validos.");
        }
        return token;
    }
    
}
